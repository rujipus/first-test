import { useEffect, memo, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

const ToDo = memo(({ toDo, removeToDoList }) => {
  const [seconds, setSeconds] = useState(5);
  const [isActive, setIsActive] = useState(false);
  
  useEffect(() => {
    console.log("🚀 ~ file: ToDo.js ~ line 5 ~ ToDo ~ toDo", toDo)
    
    const interval = setInterval(() => {
      console.log('remove to do list', toDo)
      setSeconds(seconds => seconds + 1);
      removeToDoList();
    }, 2000)
    
    return () => clearInterval(interval);
  }, [removeToDoList]);

  return <div>{toDo} {seconds} seconds have elapsed since mounting.</div>;
});

ToDo.propTypes = {
  toDo: PropTypes.string,
  removeToDoList: PropTypes.func
};

export default ToDo;
