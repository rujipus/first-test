import { memo } from 'react';
import ToDo from './ToDo';
import PropTypes from 'prop-types';

const ToDoList = memo(({ toDoList, removeToDoList }) => {
  return <>{
      toDoList?.map((toDo, index) => {
        return <ToDo key={index} toDo={toDo} removeToDoList={removeToDoList} />
    })}
  </>;
});

ToDoList.propTypes = {
  toDoList: PropTypes.arrayOf(PropTypes.string),
  removeToDoList: PropTypes.func
};

export default ToDoList;
