import { memo, useCallback, useState, Fragment } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

//components
import ToDoList from './components/ToDoList'

//mui components
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';

//mui styles
import { makeStyles, useTheme } from '@mui/styles';

const App = memo(() => {
  // const theme = useTheme();
  const classes = useStyles();
  const [toDoList, setToDoList] = useState([]);

  const validationSchema = Yup.object().shape({
    toDoText: Yup.string().required('Please enter to do list'),
  });

  const formOptions = {
    defaultValues: {
      toDoText: '',
    },
    resolver: yupResolver(validationSchema),
  };

  const { reset, handleSubmit, formState, control, setValue } = useForm(formOptions);
  const { errors } = formState;

  const removeToDoList = useCallback(
    () => {
      console.log("toDoList", toDoList)
      const newToDoList = toDoList
      console.log("newToDoList", toDoList.splice(0, 1))
      setToDoList(toDoList.splice(0, 1));
    },
    [toDoList],
  );

  const addToDoList = useCallback(
    (value) => {
      setToDoList([...toDoList, value.toDoText]);
      reset();
    },
    [toDoList, reset],
  );

  return (
    <Fragment>
      <form onSubmit={handleSubmit(addToDoList)}>
        <Grid container direction="row" justifyContent='center' paddingTop='20px' spacing={3}>
          <Grid item>
            <Controller
              name="toDoText"
              control={control}
              render={({ field: { value }, fieldState: { error } }) => (
                <TextField
                  id="toDoText"
                  label="To Do"
                  value={value}
                  inputProps={{
                    maxLength: '50',
                  }}
                  onChange={(event) => setValue('toDoText', event.target.value.trim())}
                  error={error ? true : false}
                  variant="outlined"
                  helperText={errors.toDoText?.message}
                />
              )}
            />
          </Grid>
          <Grid item >
            <Button variant="contained" type='submit' className={classes.button}>Enter</Button>
          </Grid>
        </Grid>
      </form>
      <ToDoList toDoList={toDoList} removeToDoList={removeToDoList} />
    </Fragment>
  );
});

export default App;

const useStyles = makeStyles((theme) => ({
  button: {
    width: '100px',
    height: '56px',
  },
}));
